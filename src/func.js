const getSum = (str1, str2) => {
  if (typeof str1 != "string" || typeof str2 != "string" || !Number.isInteger(+str1) || !Number.isInteger(+str2)) return false;
  else if (str1.length == 0) return str2;
  else if (str2.length == 0) return str1;
  else {
    if (str1.length > str2.length) [str1, str2] = [str2, str1];
    let array1 = str1.split("").reverse().map(x => +x);
    let array2 = str2.split("").reverse().map(x => +x);
    let str = "";
    let carry = 0;
    for (let i = 0; i < array2.length || carry; i++) {
      let sum = ~~array1[i] + array2[i] + carry;
      str += sum % 10;
      carry = carry > 9;
    }
    return str.split("").reverse().join("");
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let obj = { post: 0, comments: 0 };
  for (let i of listOfPosts) {
    if (i.author == authorName) obj.post++;
    if ("comments" in i) {
      for (let j of i.comments) {
        if (j.author == authorName) obj.comments++;
      }
    }
  }
  return `Post:${obj.post},comments:${obj.comments}`;
};

const tickets = (people) => {
  let array = [...people];
  let sum = 0;
  for (let i of array) {
    if (i == 25) sum += 25;
    else if (i == 100) sum -= 50;
  }
  return sum >= 0 ? "YES" : "NO";
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
